﻿using Autofac;
using System;
using System.Reflection;

namespace Facturador.Log.API.Modules
{
    public class ServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Facturador.Log.Domain.Services"))
                .Where(type => type.Name.EndsWith("Domain", StringComparison.Ordinal))
                .AsImplementedInterfaces();
        }
    }
}
