﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.DependencyInjection;
using System.ComponentModel;

namespace Facturador.Log.API.Modules
{
    public static partial class Bootstrapper
    {
        public static Autofac.IContainer LoadContainer(this IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ServicesModule>();
            builder.Populate(services);

            var container = builder.Build();
            return container;
        }

        public static void AddRegisterCORS(this IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("FacturadorPolicy", build =>
               build.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader()));

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("FacturadorPolicy"));
            });
        }
    }
}