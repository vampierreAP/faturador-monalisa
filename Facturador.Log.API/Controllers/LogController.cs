using Facturador.Log.Domain.ILoging;
using Facturador.Transversal.Common.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Facturador.Log.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Log")]
    public class LogController : ControllerBase
    {
       private readonly ILogDomain logDomain;

        public LogController(ILogDomain logDomain)
        {
            this.logDomain = logDomain;
        }

        [HttpPost]
        [Route("write/")]
        public ActionResult<StatusResponse<object>> Write([FromBody]LogRequest request)
        {
            var response = this.logDomain.Write(request);
            return Ok(response);
        }
    }
}
