﻿using AutoMapper;
using Facturador.Core.Domain.Inventario.Entities;
using System.Collections.Generic;
using IE = Facturador.Core.Infrastructure.Entities;

namespace Facturador.Transversal.Maps
{
    public class InventarioProfile : Profile
    {
        public InventarioProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Item, IE.Item>();
            CreateMap<IE.Item, Item>();
        }
    }
}
