﻿using Facturador.Transversal.Common.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Facturador.Core.Infrastructure.Entities
{
    [Table("item")]
    public class Item : Entity
    {
        public string Nombre  { get; set; }
        public string Descripcion { get; set; }
    }
}
