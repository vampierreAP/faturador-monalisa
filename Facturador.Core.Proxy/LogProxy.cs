﻿using Facturador.Core.IProxy;
using Facturador.Transversal.Common.Entities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Facturador.Core.Proxy
{
    public class LogProxy : ILogProxy
    {
        private readonly HttpClient httpClient;
        private readonly IConfiguration configuration;

        public LogProxy(HttpClient httpClient, IConfiguration configuration)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        public async void Write(LogRequest request)
        {
            string logURI = configuration.GetSection("Services").GetSection("LogUri").Value;
            var json = JsonConvert.SerializeObject(request);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var response = await httpClient.PostAsync(logURI + "/write", stringContent);
            response.EnsureSuccessStatusCode();
        }
    }
}
