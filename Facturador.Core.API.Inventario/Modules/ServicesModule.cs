﻿using Autofac;
using System;
using System.Linq;
using System.Reflection;

namespace Facturador.Core.API.Inventario.Modules
{
    public class ServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Facturador.Core.Domain.Inventario"))
                .Where(type => type.Name.EndsWith("Domain", StringComparison.Ordinal))
                .AsImplementedInterfaces();
        }
    }
}
