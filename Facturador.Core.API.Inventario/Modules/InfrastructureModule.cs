﻿using Autofac;
using Facturador.Core.Infraestructura.Persistence.Context;
using Facturador.Core.Infrastructure.IRepositories.Generic;
using Facturador.Core.Infrastructure.IUoW;
using Facturador.Core.Infrastructure.Repositories.Generic;
using Facturador.Core.Infrastructure.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Facturador.Core.API.Inventario.Modules
{
    public class InfrastructureModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FacturadorContext>().As<IFacturadorContext>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.Load("Facturador.Core.Infrastructure.Repositories"))
                .Where(type => type.Name.EndsWith("Repository", StringComparison.Ordinal))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}