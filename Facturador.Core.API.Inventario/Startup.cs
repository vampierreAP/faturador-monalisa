using System;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Facturador.Core.API.Inventario.GlobalErrorHandling;
using Facturador.Core.API.Inventario.Modules;
using Facturador.Core.Infraestructura.Persistence.Context;
using Facturador.Core.IProxy;
using Facturador.Transversal.Maps;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Facturador.Core.API.Inventario
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // ===== Add our DbContext ========
            services.AddDbContext<FacturadorContext>(
                options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection"))
            );

            // ===== Add Tokens ========
            Bootstrapper.AddRegisterTokens(services, Configuration);

            // ===== Auto Mapper Configurations ========
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new InventarioProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            // ===== Add CORS ========
            Bootstrapper.AddRegisterCORS(services);

            //===== Add Proxys ========
            Bootstrapper.AddRegisterProxys(services, Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //===== Add Infrastructure and Domain services ========
            var container = Bootstrapper.LoadContainer(services);
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, FacturadorContext dbContext, ILogProxy logProxy)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.ConfigureExceptionHandler(logProxy);
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();

            // ===== Create tables ======
            dbContext.Database.EnsureCreated();
        }
    }
}
