using Facturador.Core.IProxy;
using Facturador.Transversal.Common.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Facturador.Core.API.Inventario.GlobalErrorHandling
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogProxy logProxy)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>()?.Error; ;

                    if (contextFeature == null) return;

                    if (contextFeature != null)
                    {
                        var error = new StatusResponse<Exception>
                        {
                            Data = contextFeature,
                            Message = "Ocurrio un error.",
                            Success = false,
                            Value = "ERROR"
                        };
                    
                        await context.Response.WriteAsync(error.ToString());

                        logProxy.Write(new LogRequest { Message = contextFeature.ToString(), WriteType = "ERROR" });
                    }
                });
            });
        }
    }
}
