using Facturador.Transversal.Common.Entities;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Facturador.Core.API.Inventario.GlobalErrorHandling
{
  public class JsonExceptionMiddleware
  {
    public async Task<StatusResponse<Exception>> Invoke(HttpContext context)
    {
      context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

      var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;
      

      var error = new StatusResponse<Exception>
      {
        Data = ex,
        Message = "Ocurrio un error.",
        Success = false,
        Value = "ERROR"
      };

      context.Response.ContentType = "application/json";

      //using (var writer = new StreamWriter(context.Response.Body))
      //{
      //  new JsonSerializer().Serialize(writer, error);
      //  await writer.FlushAsync().ConfigureAwait(false);
      //}

      return error;
    }
  }
}
