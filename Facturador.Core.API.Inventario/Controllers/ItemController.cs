using System.Collections.Generic;
using Facturador.Core.Domain.IInventario;
using Facturador.Core.Domain.Inventario.Entities;
using Facturador.Transversal.Common.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Facturador.Core.API.Inventario.Controllers
{
    [Produces("application/json")]
    [Route("api/item")]
    [Authorize]
    public class ItemController : ControllerBase
    {
        private readonly IItemDomain itemDomain;
        public ItemController(IItemDomain itemDomain)
        {
            this.itemDomain = itemDomain;
        }

        [HttpPost]
        [Route("adicionar/")]
        public ActionResult<StatusResponse<Item>> Adicionar([FromBody] Item item)
        {
            var response = itemDomain.Adicionar(item);
            return Ok(response);
        }

        [HttpGet]
        [Route("listar/")]
        public ActionResult<StatusResponse<List<Item>>> Listar()
        {
            var response = itemDomain.Listar();
            return Ok(response);
        }
    }
}
