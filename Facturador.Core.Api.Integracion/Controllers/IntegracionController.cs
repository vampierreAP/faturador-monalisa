﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Facturador.Core.Api.Integracion.Controllers
{
    [Produces("application/json")]
    [Route("api/integracion")]
    [EnableCors("FacturadorPolicy")]
    public class IntegracionController : ControllerBase
    {
        public IntegracionController()
        {

        }
    }
}