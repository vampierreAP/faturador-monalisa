﻿using Facturador.Transversal.Common.Entities;
using System;

namespace Facturador.Log.Domain.ILoging
{
    public interface ILogDomain
    {
        StatusResponse<object> Write(LogRequest request);
    }
}
