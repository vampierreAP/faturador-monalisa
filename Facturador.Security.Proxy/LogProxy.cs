﻿using Facturador.Security.IProxy;
using Facturador.Transversal.Common.Entities;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Configuration;
using System;

namespace Facturador.Security.Proxy
{
    public class LogProxy : ILogProxy
    {
        private readonly HttpClient httpClient;
        private readonly IConfiguration configuration;

        public LogProxy(HttpClient httpClient, IConfiguration configuration)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        public async void Write(LogRequest request)
        {
            string logURI = configuration.GetSection("Services").GetSection("LogUri").Value;
            var json = JsonConvert.SerializeObject(request);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var response = await httpClient.PostAsync(logURI + "/write", stringContent);
            response.EnsureSuccessStatusCode();
        }
    }
}
