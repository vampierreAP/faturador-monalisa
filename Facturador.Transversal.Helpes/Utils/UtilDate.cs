﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facturador.Transversal.Helpers.Utils
{
    public static class UtilDate
    {
        public static string GetHoursMinutes(DateTime fecha)
        {
            StringBuilder sb = new StringBuilder();
            if (fecha.Hour < 10) sb.Append("0");
            sb.Append(fecha.Hour);
            sb.Append(":");
            if (fecha.Minute < 10) sb.Append("0");
            sb.Append(fecha.Minute);
            return sb.ToString();
        }

        public static bool ValidateDateInRange(DateTime fechaEval, DateTime fechaIni, DateTime fechaFin)
        {
            fechaEval = new DateTime(fechaEval.Year, fechaEval.Month, fechaEval.Day, 0, 0, 0);
            fechaIni = new DateTime(fechaIni.Year, fechaIni.Month, fechaIni.Day, 0, 0, 0);
            fechaFin = new DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, 0, 0, 0);

            return fechaEval.CompareTo(fechaIni) >= 0 && fechaEval.CompareTo(fechaFin) <= 0;
        }

    }
}
