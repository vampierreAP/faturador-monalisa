﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Facturador.Transversal.Helpers.Utils
{
    public static class UtilString
    {
        public static string RemoveDiacritics(this String s)
        {
            s = s.ToLower().Trim();
            String normalizedString = s.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < normalizedString.Length; i++)
            {
                Char c = normalizedString[i];
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    stringBuilder.Append(c);
            }

            return stringBuilder.ToString();
        }
    }
}
