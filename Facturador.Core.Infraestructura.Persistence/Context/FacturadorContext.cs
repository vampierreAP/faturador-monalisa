﻿using Facturador.Core.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace Facturador.Core.Infraestructura.Persistence.Context
{
    public class FacturadorContext : DbContext, IFacturadorContext
    {
        public FacturadorContext(DbContextOptions<FacturadorContext> options) : base(options)
        {
            //DropCreateDatabaseIfModelChanges<TContext>.Seed() DropCreateDatabaseIfModelChanges
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Database.EnsureCreated();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Item> Item { get; set; }

    }
}