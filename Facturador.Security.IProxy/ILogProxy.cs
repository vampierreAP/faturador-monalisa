﻿using Facturador.Transversal.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facturador.Security.IProxy
{
    public interface ILogProxy
    {
        void Write(LogRequest request);
    }
}
