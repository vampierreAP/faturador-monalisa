﻿using Facturador.Transversal.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facturador.Core.Domain.Inventario.Entities
{
    public class Item
    {
        //public Guid Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
