﻿using Facturador.Core.Domain.IInventario;
using Facturador.Core.Infrastructure.IRepositories.Generic;
using Facturador.Core.Infrastructure.IUoW;
using Facturador.Transversal.Common.Entities;
using Facturador.Core.Domain.Inventario.Entities;
using IE = Facturador.Core.Infrastructure.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Facturador.Transversal.Helpers.Utils;

namespace Facturador.Core.Domain.Inventario
{
    public class ItemDomain : IItemDomain
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository<IE.Item> itemRepository;
        public ItemDomain(IMapper mapper,
            IUnitOfWork unitOfWork,
            IRepository<IE.Item> itemRepository)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.itemRepository = itemRepository;
        }

        public StatusResponse<Item> Adicionar(Item item)
        {
            StatusResponse<Item> statusResponse;
            
            var itemMap = mapper.Map<IE.Item>(item);
            var entity = itemRepository.Add(itemMap);
            this.unitOfWork.Commit();

            statusResponse = new StatusResponse<Item>
            {
                Success = true,
                Message = "Adicionado Satisfactoriamente.",
                Data = mapper.Map<Item>(entity)
            };

            return statusResponse;
        }

        public StatusResponse<List<Item>> Listar()
        {
            StatusResponse<List<Item>> statusResponse;            
            var entities = itemRepository.GetAll().ToList();

            statusResponse = new StatusResponse<List<Item>>
            {
                Success = true,
                Message = "Ejecutado Satisfactoriamente.",
                Data = mapper.Map<List<Item>>(entities)
            };
            return statusResponse;
        }
    }
}
