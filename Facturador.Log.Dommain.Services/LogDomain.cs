﻿using Facturador.Log.Domain.ILoging;
using Facturador.Transversal.Common.Entities;
using Microsoft.Extensions.Logging;

namespace Facturador.Log.Domain.Loging
{
    public class LogDomain : ILogDomain
    {
        private readonly ILogger _logger;
        public LogDomain(ILogger<LogDomain> logger)
        {
            _logger = logger;
        }
        
        public StatusResponse<object> Write(LogRequest request)
        {                
            try
            {
                if (request.WriteType == "INFO")
                {
                    _logger.LogInformation(request.Message);
                }
                else
                {
                    //Helper.Sentry.Write(new System.Exception(request.Message));
                    _logger.LogError(request.Message);
                }

                StatusResponse<object> response = new StatusResponse<object>()
                {
                    Message = "Register log",
                    Success = true
                };

                return response;
            }
            catch (System.Exception)
            {
                StatusResponse<object> response = new StatusResponse<object>()
                {
                    Message = "Error log",
                    Success = true
                };

                return response;
            }
        }

}
}
