﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facturador.Transversal.Common.Entities
{
    public class StatusResponse<T> : StatusResponse
    {
        public T Data { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class StatusResponse 
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Value { get; set; }
        public List<string> Messages { get; set; }
    }


}
