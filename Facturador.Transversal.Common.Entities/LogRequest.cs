using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facturador.Transversal.Common.Entities
{
    public class LogRequest
    {
        public string WriteType { get; set; }
        public string Message { get; set; }

  }
}
