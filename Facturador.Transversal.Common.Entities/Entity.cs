using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Facturador.Transversal.Common.Entities
{
    public class Entity
    {
        [Key]
        public string Id { get; set; }

        //[ScaffoldColumn(false)]
        //[Column(TypeName = "DateTime")]
        //public DateTime FechaCreacion { get; set; }

        //[ScaffoldColumn(false)]
        //public Guid CreadoPor { get; set; }

        //[ScaffoldColumn(false)]
        //[Column(TypeName = "DateTime")]
        //public DateTime? FechaActualziacion { get; set; }

        //[ScaffoldColumn(false)]
        //public Guid? ActualizadoPor { get; set; }

        //[ScaffoldColumn(false)]
        //[Column(TypeName = "DateTime")]
        //public DateTime? FechaEliminacion { get; set; }

        //[ScaffoldColumn(false)]
        //public Guid? EliminadoPor { get; set; }

        //[ScaffoldColumn(false)]
        //public bool EsActivo { get; set; }

        protected Entity()
        {
            //Id = Guid.NewGuid();
            //FechaCreacion = DateTime.Now.ToLocalTime();
            //CreadoPor = Guid.NewGuid(); //USUARIO EN SESION
            //EsActivo = true;
        }
    }
}
