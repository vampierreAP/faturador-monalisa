using System;
using Autofac.Extensions.DependencyInjection;
using Facturador.Security.API.GlobalErrorHandling;
using Facturador.Security.API.Modules;
using Facturador.Security.Infrastructure.Data;
using Facturador.Security.IProxy;
using Facturador.Security.Proxy;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Facturador.Security.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // ===== Add DbContext ========
            Bootstrapper.AddRegisterContext(services, Configuration);

            // ===== Add Identity ========
            Bootstrapper.AddRegisterIdentityServer(services);

            // ===== Add Tokens ========
            Bootstrapper.AddRegisterTokens(services, Configuration);

            // ===== Add CORS ========
            Bootstrapper.AddRegisterCORS(services);

            //===== Add Proxys ========
            Bootstrapper.AddRegisterProxys(services, Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
           
            //===== Add Domain services ========
            var container = Bootstrapper.LoadContainer(services);

            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IdentityContext dbContext, ILogProxy logProxy)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.ConfigureExceptionHandler(logProxy);
            app.UseHttpsRedirection();
            app.UseMvc();

            // ===== Create tables ======
            dbContext.Database.EnsureCreated();
        }
    }
}
