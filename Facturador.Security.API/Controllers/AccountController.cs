using System.Threading.Tasks;
using Facturador.Security.Domain.Entities;
using Facturador.Security.Domain.IServices;
using Facturador.Security.IProxy;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Facturador.Security.API.Controllers
{
    [Produces("application/json")]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountDomain accountDomain;
        public AccountController(IAccountDomain accountDomain)
        {
            this.accountDomain = accountDomain;
        }

        [HttpPost]
        [Route("login/")]
        public async Task<object> Login([FromBody] User model)
        {
            var result = await accountDomain.Login(model);
            return result;
        }

        [HttpPost]
        [Route("register/")]
        public async Task<object> Register([FromBody] User model)
        {
            var result = await accountDomain.Register(model);
            return result;
        }
    }
}
