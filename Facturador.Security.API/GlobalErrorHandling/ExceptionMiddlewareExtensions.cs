using Facturador.Security.IProxy;
using Facturador.Transversal.Common.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;

namespace Facturador.Security.API.GlobalErrorHandling
{
  public static class ExceptionMiddlewareExtensions
  {    
    public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogProxy logProxy)
{
      app.UseExceptionHandler(appError =>
      {
        appError.Run(async context =>
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ContentType = "application/json";
            
            var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
            if (contextFeature != null)
            {
                var error = new StatusResponse<Exception>
                {
                    Data = contextFeature.Error,
                    Message = "Ocurrio un error.",
                    Success = false,
                    Value = "ERROR"
                };
                await context.Response.WriteAsync(error.ToString());

                logProxy.Write(new LogRequest { Message = contextFeature.Error.ToString(), WriteType = "ERROR" });
            }
        });
      });
    }
  }
}
