﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Facturador.Security.Infrastructure.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Facturador.Security.API.Modules
{
    public static partial class Bootstrapper
    {
        public static Autofac.IContainer LoadContainer(this IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ServicesModule>();
            builder.Populate(services);

            var container = builder.Build();
            return container;
        }

        public static void AddRegisterContext(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<IdentityContext>(
                options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection"))
            );
        }

        public static void AddRegisterIdentityServer(this IServiceCollection services)
        {
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                // User settings
                options.User.RequireUniqueEmail = true;
            });
        }

        public static void AddRegisterCORS(this IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("FacturadorPolicy", build =>
               build.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader()));

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("FacturadorPolicy"));
            });
        }

        public static void AddRegisterTokens(this IServiceCollection services, IConfiguration Configuration)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Auth:Jwt:Key"]));
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = Configuration["Auth:Jwt:Issuer"],
                        ValidAudience = Configuration["Auth:Jwt:Audience"],
                        IssuerSigningKey = signingKey
                    };

                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            Console.WriteLine("OnAuthenticationFailed: " + context.Exception.Message);
                            return Task.CompletedTask;
                        },
                        OnTokenValidated = context =>
                        {
                            Console.WriteLine("OnTokenValidated: " + context.SecurityToken);
                            return Task.CompletedTask;
                        }
                    };
                });
        }
    }
}
