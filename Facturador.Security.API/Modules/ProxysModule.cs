﻿using Facturador.Security.IProxy;
using Facturador.Security.Proxy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using System.Net.Http;

namespace Facturador.Security.API.Modules
{
    public static partial class Bootstrapper
    {
        public static void AddRegisterProxys(this IServiceCollection services, IConfiguration Configuration)
        {
            var retryPolicy = HttpPolicyExtensions.HandleTransientHttpError().RetryAsync(int.Parse(Configuration["Polly:MaxTry"]));
            var noOp = Policy.NoOpAsync().AsAsyncPolicy<HttpResponseMessage>();

            services.AddHttpClient<ILogProxy, LogProxy>()
                .AddPolicyHandler(request => request.Method == HttpMethod.Get ? retryPolicy : noOp)
                .AddPolicyHandler(Policy.TimeoutAsync<HttpResponseMessage>(int.Parse(Configuration["Polly:TimeOut"])));
        }
    }
}
