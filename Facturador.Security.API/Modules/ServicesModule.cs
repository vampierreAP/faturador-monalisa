﻿using Autofac;
using System;
using System.Linq;
using System.Reflection;

namespace Facturador.Security.API.Modules
{
    public class ServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Facturador.Security.Domain.Services"))
                .Where(type => type.Name.EndsWith("Domain", StringComparison.Ordinal))
                .AsImplementedInterfaces();
        }
    }
}
