﻿using Facturador.Core.Infraestructura.Persistence.Context;
using Facturador.Core.Infrastructure.IRepositories.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Facturador.Core.Infrastructure.IUoW
{
    public interface IUnitOfWork : IDisposable
    {
        IFacturadorContext Context { get; }
        int Commit();
        /// <returns>The number of objects in an Added, Modified, or Deleted state asynchronously</returns>
        Task<int> CommitAsync();
        /// <returns>Repository</returns>
        //IRepository<TEntity> GetRepository<TEntity>()
        //    where TEntity : class;
    }
}