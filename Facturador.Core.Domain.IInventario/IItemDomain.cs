﻿using Facturador.Transversal.Common.Entities;
using Facturador.Core.Domain.Inventario.Entities;
using System.Collections.Generic;

namespace Facturador.Core.Domain.IInventario
{
    public interface IItemDomain
    {
        StatusResponse<Item> Adicionar(Item entidad);
        StatusResponse<List<Item>> Listar();
    }
}
