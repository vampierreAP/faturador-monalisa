﻿using Facturador.Security.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Facturador.Security.Domain.IServices
{
    public interface IAccountDomain
    {
        Task<object> Register(User model);
        Task<object> Login(User model);
    }
}
