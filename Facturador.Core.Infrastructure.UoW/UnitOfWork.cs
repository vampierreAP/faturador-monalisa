﻿using Facturador.Core.Infraestructura.Persistence.Context;
using Facturador.Core.Infrastructure.IRepositories.Generic;
using Facturador.Core.Infrastructure.IUoW;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Facturador.Core.Infrastructure.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private IFacturadorContext dbContext;
        
        public UnitOfWork(IFacturadorContext dbContext)
        {
            this.dbContext = dbContext;
        }
  
        /// <returns>The number of objects in an Added, Modified, or Deleted state</returns>
        public int Commit()
        {
            var response = dbContext.SaveChanges();
            return response;
        }
        public async Task<int> CommitAsync()
        {
            return await dbContext.SaveChangesAsync(CancellationToken.None);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(obj: this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dbContext != null)
                {
                    dbContext.Dispose();
                    dbContext = null;
                }
            }
        }

        public IFacturadorContext Context => dbContext;
    }
}