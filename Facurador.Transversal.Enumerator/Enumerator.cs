﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facurador.Transversal.Enumerator
{
    public class Enumerator
    {
        public enum Tipo_Documento : int
        {
            DNI = 1,
            CARNET_EXTRANJERIA = 2
        }
    }
}
