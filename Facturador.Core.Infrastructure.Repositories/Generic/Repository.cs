﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Facturador.Core.Infraestructura.Persistence.Context;
using Facturador.Core.Infrastructure.IRepositories.Generic;
using Facturador.Core.Infrastructure.IUoW;
using Microsoft.EntityFrameworkCore;

namespace Facturador.Core.Infrastructure.Repositories.Generic
{
    public class Repository<T> :  IRepository<T> where T : class
    {
        private readonly IUnitOfWork uow;
        private readonly DbSet<T> dbSet;

        public Repository(IUnitOfWork uow)
        {
            this.uow = uow;
            dbSet = this.uow.Context.Set<T>();
        }

        public virtual T Add(T entity)
        {
            var newEntity = dbSet.Add(entity).Entity;
            return newEntity;
        }

        public T Get<TKey>(TKey id)
        {
            return dbSet.Find(id);
        }

        public async Task<T> GetAsync<TKey>(TKey id)
        {
            return await dbSet.FindAsync(id);
        }

        public T Get(params object[] keyValues)
        {
            return dbSet.Find(keyValues);
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return dbSet.Where(predicate);
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate, string include)
        {
            return FindBy(predicate).Include(include);
        }

        public IQueryable<T> GetAll()
        {
            return dbSet;
        }

        public IQueryable<T> GetAll(int page, int pageCount)
        {
            var pageSize = (page - 1) * pageCount;

            return dbSet.Skip(pageSize).Take(pageCount);
        }

        public IQueryable<T> GetAll(string include)
        {
            return dbSet.Include(include);
        }

        public IQueryable<T> GetAll(string include, string include2)
        {
            return dbSet.Include(include).Include(include2);
        }

        public bool Exists(Expression<Func<T, bool>> predicate)
        {
            return dbSet.Any(predicate);
        }

        public virtual T SoftDelete(T entity)
        {
            entity.GetType().GetProperty("EsActivo")?.SetValue(entity, false);
            return dbSet.Update(entity).Entity;
        }

        public virtual void HardDelete(T entity)
        {
            this.dbSet.Remove(entity);
        }
        public virtual T Update(T entity)
        {
            return dbSet.Update(entity).Entity;
        }
    }
}
